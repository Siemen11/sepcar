#include "party.hpp"

const unsigned char s_box[256] =   {
        //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

// We start by reserving some memory for x and a.
Party::Party() {
    x.reserve(400);
    a.reserve(400);
}

// The three parties are syncronised to make the MPC operations between themselves.
// In this function we also set the keys k1 and k2 that are used to make correlated randomness.
void Party::sync(Party* party1, Party* party2) {
    simulate_communication(INTERNAL_DELAY);

    counter = 0;
    big_counter = 0;

    first_party  = party1;
    second_party = party2;
    k1 = (unsigned int) rand();
    second_party->setk2(k1);
}

// Setter for k2.
void Party::setk2(unsigned int k) {
    k2 = k;
}

// Add a share from a character value. These shares are added to all three parties.
unsigned long Party::addShare(unsigned char value) {
    simulate_communication(INTERNAL_DELAY);

    unsigned char r[3];

    r[0] = (unsigned char) (rand() & 0xFF);
    r[1] = (unsigned char) (rand() & 0xFF);
    r[2] = r[0]^r[1];

    addShare(r[0], r[2]^value);
    first_party->addShare(r[1], r[0]^value);
    return second_party->addShare(r[2], r[1]^value);
}

// Add the actual share to the arrays of the party.
unsigned long Party::addShare(unsigned char x_temp, unsigned char a_temp){
    x.push_back(x_temp);
    a.push_back(a_temp);
    unsigned long size = x.size()-1;
    return size;
}

// Set the shares at location index to zero
void Party::clearSharelocal(unsigned long index) {
    x[index] = 0;
    a[index] = 0;
}

// Call clearSharelocal over all three parties.
void Party::clearShare(unsigned long index) {
    clearSharelocal(index);
    first_party->clearSharelocal(index);
    second_party->clearSharelocal(index);
}

// copy the contents of the shares at location index to location index3.
unsigned long Party::copySharelocal(unsigned long index, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index];
        a[*index3] = a[index];
        return *index3;
    } else {
        x.push_back(x[index]);
        a.push_back(a[index]);
        return x.size() - 1;
    }
}

// Call copySharelocal over all three parties.
void Party::copyShare(unsigned long index, unsigned long* index3) {
    copySharelocal(index, index3);
    first_party->copySharelocal(index, index3);
    *index3 = second_party->copySharelocal(index, index3);
}

// Refill the array random by new randomness by calculating two AES calls.
void Party::refillRandom() {
    uint8_t result1[16], result2[16], M[16], K1[16], K2[16];

    for (int i = 0; i < 16; i++) {
        M[i] = 0;
        K1[i] = (unsigned char)(((big_counter+k1)>>(8*i))&0xFF);
        K2[i] = (unsigned char)(((big_counter+k2)>>(8*i))&0xFF);
    }

    AES_encrypt(M, K1, result1);
    for (int i = 0; i < 16; i++) {
        random[i] = result1[i];
    }

    AES_encrypt(M, K2, result2);
    for (int i = 0; i < 16; i++) {
        other_random[i] = result2[i];
        random[i] = random[i] ^ other_random[i];
    }
    big_counter++;
    counter = 0;
}

// Get a correlated random character. Thus the XOR of these characters between the three parties is equal to zero.
unsigned char Party::getRandom() {
    if (counter % 16 == 0) {
        refillRandom();
    }
    unsigned char result = random[counter];
    ++counter;
    return result;
}

unsigned long Party::shareRandlocal(unsigned long index3) {
    if (counter % 16 == 0) {
        refillRandom();
    }

    if (index3) {
        x[index3] = random[counter] ;
        a[index3] = other_random[counter];
        ++counter;
        return index3;
    } else {
        x.push_back(random[counter]);
        a.push_back(other_random[counter]);
        ++counter;
        return x.size() - 1;
    }

}

void Party::shareRand(unsigned long* index3) {
    shareRandlocal(*index3);
    first_party->shareRandlocal(*index3);
    *index3 = second_party->shareRandlocal(*index3);
}

// Set the share at index to first and second.
void Party::setShare(unsigned long index, unsigned char first, unsigned char second) {
    x[index] = first;
    a[index] = second;
}

// Get the x share at index.
unsigned char Party::getSharex(unsigned long index) {
    return x[index];
}

// Get the a share at index.
unsigned char Party::getSharea(unsigned long index) {
    return a[index];
}

// Set the first bit of the character equal to its ith bit. Set the rest of the bits to zero.
unsigned long Party::partlocal(unsigned long index1, unsigned char i, unsigned long* index3) {
    if (*index3) {
        x[*index3] = (unsigned char)((x[index1]>>i)&1);
        a[*index3] = (unsigned char)((a[index1]>>i)&1);
        return *index3;
    } else {
        x.push_back((unsigned char)((x[index1]>>i)&1));
        a.push_back((unsigned char)((a[index1]>>i)&1));
        return x.size() - 1;
    }
}

// Call partlocal over all three parties.
void Party::part(unsigned long index1, unsigned char i, unsigned long* index3) {
    partlocal(index1, i, index3);
    first_party->partlocal(index1, i, index3);
    *index3 = second_party->partlocal(index1, i, index3);
}

// Take the first bit in the character and set all bits in the character to that bit.
unsigned long Party::lengthenlocal(unsigned long index1, unsigned long* index3) {
    if (*index3) {
        if (x[index1]&1) {
            x[*index3] = 255;
        } else {
            x[*index3] = 0;
        }
        if (a[index1]&1) {
            a[*index3] = 255;
        } else {
            a[*index3] = 0;
        }
        return *index3;
    } else {
        if (x[index1]&1) {
            x.push_back(255);
        } else {
            x.push_back(0);
        }
        if (a[index1]&1) {
            a.push_back(255);
        } else {
            a.push_back(0);
        }
        return x.size() - 1;
    }
}

// Call lengthenlocal over all three parties.
void Party::lengthen(unsigned long index1, unsigned long* index3) {
    lengthenlocal(index1, index3);
    first_party->lengthenlocal(index1, index3);
    *index3 = second_party->lengthenlocal(index1, index3);
}

// XOR two shares and save it in index3.
unsigned long Party::XORlocal(unsigned long index1, unsigned long index2, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index1] ^ x[index2];
        a[*index3] = a[index1] ^ a[index2];
        return *index3;
    } else {
        x.push_back(x[index1] ^ x[index2]);
        a.push_back(a[index1] ^ a[index2]);
        return x.size() - 1;
    }
}

// Call XORlocal over all three parties.
void Party::XOR(unsigned long index1, unsigned long index2, unsigned long* index3) {
    XORlocal(index1, index2, index3);
    first_party->XORlocal(index1, index2, index3);
    *index3 = second_party->XORlocal(index1, index2, index3);
}

// XOR a shares with a constant and save it in index3.
unsigned long Party::XORlocalcons(unsigned long index1, unsigned char cons, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index1] ;
        a[*index3] = a[index1] ^ cons;
        return *index3;
    } else {
        x.push_back(x[index1]);
        a.push_back(a[index1] ^ cons);
        return x.size() - 1;
    }
}

// Call XORlocalcons over all three parties.
void Party::XORcons(unsigned long index1, unsigned char cons, unsigned long* index3) {
    XORlocalcons(index1, cons, index3);
    first_party->XORlocalcons(index1, cons, index3);
    *index3 = second_party->XORlocalcons(index1, cons, index3);
}

// The first step to calculate the AND between two shares as described in the paper.
unsigned long Party::getInterAND(unsigned long index1, unsigned long index2, unsigned long index3) {
    if (index3) {
        a[index3] = (x[index1] & x[index2]) ^ (a[index1] & a[index2]) ^ getRandom();
        x[index3] = 0;
        return(index3);
    } else {
        a.push_back((x[index1] & x[index2]) ^ (a[index1] & a[index2]) ^ getRandom());
        x.push_back(0);
        return x.size() - 1;
    }
}

// The second step to calculate the AND between two shares as described in the paper.
void Party::ANDlocal(unsigned long index3) {
    unsigned char us = a[index3];

    x[index3] = (us ^ second_party->getSharea(index3));
}

// Let all three parties work together to calculate the AND gate.
void Party::AND(unsigned long index1, unsigned long index2, unsigned long *index3) {
    simulate_communication(INTERNAL_DELAY);

    getInterAND(index1, index2, *index3);
    first_party->getInterAND(index1, index2, *index3);
    *index3 = second_party->getInterAND(index1, index2, *index3);

    this->ANDlocal(*index3);
    first_party->ANDlocal(*index3);
    second_party->ANDlocal(*index3);
}

// AND a share with a constant and save it in index3.
unsigned long Party::ANDlocalcons(unsigned long index1, unsigned char cons, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index1] & cons;
        a[*index3] = a[index1] & cons;
        return *index3;
    } else {
        x.push_back(x[index1] & cons);
        a.push_back(a[index1] & cons);
        return x.size() - 1;
    }
}

// Call ANDlocalcons over all three parties.
void Party::ANDcons(unsigned long index1, unsigned char cons, unsigned long* index3) {
    ANDlocalcons(index1, cons, index3);
    first_party->ANDlocalcons(index1, cons, index3);
    *index3 = second_party->ANDlocalcons(index1, cons, index3);
}

// NOT a share and save it in index3.
unsigned long Party::NOTlocal(unsigned long index, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index];
        a[*index3] = ~a[index];
        return *index3;
    } else {
        x.push_back(x[index]);
        unsigned char temp = ~(a[index]);
        a.push_back(temp);
        return x.size() - 1;
    }
}

// Call NOTlocal over all three parties.
void Party::NOT(unsigned long index, unsigned long* index3) {
    NOTlocal(index, index3);
    first_party->NOTlocal(index, index3);
    *index3 = second_party->NOTlocal(index, index3);
}

void Party::getInterEq(unsigned long index1, unsigned char offset, unsigned long index3) {
    bool inter = (bool)((((x[index1]>>2*offset)&1) & ((x[index1]>>(2*offset+1))&1)) ^ (((a[index1]>>2*offset)&1) & ((a[index1]>>(2*offset+1))&1)) ^ (getRandom()&1));
    a[index3] &= ~(1<<2*offset);
    a[index3] &= ~(1<<(2*offset+1));
    a[index3] |= (inter<<offset);
    x[index3] = 0;
}

void Party::Eqlocal(unsigned long index3) {
    unsigned char us = a[index3];

    x[index3] = (us ^ second_party->getSharea(index3));
}

// Determine if two shares hold an equal value and set the result in the first bit of index3.
void Party::Eq(unsigned long index1, unsigned long index2, unsigned long* index3) {
    XOR(index1, index2, index3);
    NOT(*index3, index3);

    {
        simulate_communication(INTERNAL_DELAY);
        for (unsigned char i = 0; i < 4; i++) {
            getInterEq(*index3, i, *index3);
            first_party->getInterEq(*index3, i, *index3);
            second_party->getInterEq(*index3, i, *index3);

            this->Eqlocal(*index3);
            first_party->Eqlocal(*index3);
            second_party->Eqlocal(*index3);

        }

    }

    {
        simulate_communication(INTERNAL_DELAY);
        for (unsigned char i = 0; i < 2; i++) {
            getInterEq(*index3, i, *index3);
            first_party->getInterEq(*index3, i, *index3);
            second_party->getInterEq(*index3, i, *index3);

            this->Eqlocal(*index3);
            first_party->Eqlocal(*index3);
            second_party->Eqlocal(*index3);
        }

    }

    {
        simulate_communication(INTERNAL_DELAY);
        getInterEq(*index3, 0, *index3);
        first_party->getInterEq(*index3, 0, *index3);
        second_party->getInterEq(*index3, 0, *index3);

        this->Eqlocal(*index3);
        first_party->Eqlocal(*index3);
        second_party->Eqlocal(*index3);

    }
}

// Determine if a share is equal to a constant and set the result in the first bit of index3.
void Party::Eqcons(unsigned long index1, unsigned char cons, unsigned long* index3) {
    XORcons(index1, cons, index3);
    NOT(*index3, index3);

    {
        simulate_communication(INTERNAL_DELAY);
        for (unsigned char i = 0; i < 4; i++) {
            getInterEq(*index3, i, *index3);
            first_party->getInterEq(*index3, i, *index3);
            second_party->getInterEq(*index3, i, *index3);

            this->Eqlocal(*index3);
            first_party->Eqlocal(*index3);
            second_party->Eqlocal(*index3);

        }

    }

    {
        simulate_communication(INTERNAL_DELAY);
        for (unsigned char i = 0; i < 2; i++) {
            getInterEq(*index3, i, *index3);
            first_party->getInterEq(*index3, i, *index3);
            second_party->getInterEq(*index3, i, *index3);

            this->Eqlocal(*index3);
            first_party->Eqlocal(*index3);
            second_party->Eqlocal(*index3);
        }

    }

    {
        simulate_communication(INTERNAL_DELAY);
        getInterEq(*index3, 0, *index3);
        first_party->getInterEq(*index3, 0, *index3);
        second_party->getInterEq(*index3, 0, *index3);

        this->Eqlocal(*index3);
        first_party->Eqlocal(*index3);
        second_party->Eqlocal(*index3);

    }
}

// Perform a left shift on a share and put the result in index3.
unsigned long Party::LSlocal(unsigned long index, unsigned char shift, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index]<<shift;
        a[*index3] = a[index]<<shift;
        return *index3;
    } else {
        x.push_back(x[index]<<shift);
        a.push_back(a[index]<<shift);
        return x.size() - 1;
    }
}

// Call LSlocal over all three parties.
void Party::LS(unsigned long index, unsigned char shift, unsigned long* index3) {
    if (shift > 8) {
        shift = 8;
    }
    LSlocal(index, shift, index3);
    first_party->LSlocal(index, shift, index3);
    *index3 = second_party->LSlocal(index, shift, index3);
}

// Perform a right shift on a share and put the result in index3.
unsigned long Party::RSlocal(unsigned long index, unsigned char shift, unsigned long* index3) {
    if (*index3) {
        x[*index3] = x[index]>>shift;
        a[*index3] = a[index]>>shift;
        return *index3;
    } else {
        x.push_back(x[index]>>shift);
        a.push_back(a[index]>>shift);
        return x.size() - 1;
    }
}

// Call RSlocal over all three parties.
void Party::RS(unsigned long index, unsigned char shift, unsigned long* index3) {
    if (shift > 8) {
        shift = 8;
    }
    RSlocal(index, shift, index3);
    first_party->RSlocal(index, shift, index3);
    *index3 = second_party->RSlocal(index, shift, index3);
}

// Calculate a finite field multiplication between two shares and store the result in index3.
void Party::FFMult(unsigned long index1, unsigned long index2, unsigned long *index3) {
    if (*index3 == 0 || *index3 == index2 || *index3 == index1) {
        *index3 = addShare(0, 0);
        first_party->addShare(0, 0);
        second_party->addShare(0, 0);
    } else {
        clearShare(*index3);
    }
    unsigned long other = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);
    unsigned long temp = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);

    unsigned long inters[8];
    for(int i = 0; i < 8; i++) {
        inters[i] = addShare(0, 0);
        first_party->addShare(0, 0);
        second_party->addShare(0, 0);
    }

    XOR(index1, inters[0], &inters[0]);

    for (unsigned char i = 1; i < 8; i++) {
        part(inters[i-1], 7, &temp);
        lengthen(temp, &temp);
        ANDcons(temp, 27, &temp);
        LS(inters[i-1], 1, &inters[i]);
        XOR(inters[i], temp, &inters[i]);
    }
    XOR(index2, other, &other);

    simulate_communication(INTERNAL_DELAY); // Only one round of communication is needed

    // These ANDs can be done in parallel
    for (unsigned char i = 0; i < 8; i++) {

        part(other, i, &temp);
        lengthen(temp, &temp);

        getInterAND(inters[i], temp, temp);
        first_party->getInterAND(inters[i], temp, temp);
        temp = second_party->getInterAND(inters[i], temp, temp);

        this->ANDlocal(temp);
        first_party->ANDlocal(temp);
        second_party->ANDlocal(temp);

        XOR(*index3, temp, index3);

    }
}

// Calculate a finite field multiplication between share and a constant and store it in index3.
void Party::FFMultcons(unsigned long index1, unsigned char cons, unsigned long *index3) {
    if (*index3 == 0 || *index3 == index1) {
        *index3 = addShare(0, 0);
        first_party->addShare(0, 0);
        second_party->addShare(0, 0);
    } else {
        clearShare(*index3);
    }
    unsigned long inter = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);
    unsigned long temp = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);

    XOR(index1, inter, &inter);

    while(cons) {
        if (cons & 1) {
            XOR(*index3, inter, index3);
        }

        part(inter, 7, &temp);
        lengthen(temp, &temp);
        ANDcons(temp, 27, &temp);
        LS(inter, 1, &inter);
        XOR(inter, temp, &inter);

        cons >>= 1;
    }
}

// Calculate the modular inverse (mod 2^8) of a share and put it in index3.
void Party::power254(unsigned long index, unsigned long* index3) {
    unsigned long b = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);

    unsigned long temp = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);

    unsigned long random = 0;

    unsigned char inver = 0;

    Eqcons(index, 0, &temp); // 3 com calls
    XOR(b, temp, &b);

    while(inver == 0) {

        shareRand(&random);

        XOR(index, b, &temp);
        FFMult(random, temp, &temp); // 1 com call
        inver = open(temp); // 1 com call

    }
    inver = exp254(inver);

    FFMultcons(random, inver, index3);
    XOR(*index3, b, index3);
}

// Calculate a vector multiplication between a share and a vector and store the result in index3.
// The vector multipication is done via AND gates and XOR gates.
void Party::vectorMult(unsigned long index, unsigned char* vector, unsigned long* index3) {
    if (*index3 == 0 || *index3 == index) {
        *index3 = addShare(0, 0);
        first_party->addShare(0, 0);
        second_party->addShare(0, 0);
    } else {
        clearShare(*index3);
    }
    unsigned long inter = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);
    unsigned long temp = addShare(0, 0);
    first_party->addShare(0, 0);
    second_party->addShare(0, 0);

    for (unsigned char i = 0; i < 8; i++) {
        ANDcons(index, vector[i], &inter);

        for (unsigned char j = 0; j < 8; j++) {
            part(inter, j, &temp);
            LS(temp, i, &temp);
            XOR(*index3, temp, index3);
        }
    }
}

// Open the share at the index.
unsigned char Party::open(unsigned long index) {
    simulate_communication(INTERNAL_DELAY);

    unsigned char inter = a[index] ^ second_party->getSharex(index);

    return inter;
}
