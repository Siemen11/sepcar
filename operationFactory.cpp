#include "operationFactory.hpp"

mutex mtxopp;
// This file contain functions which can be parallelised. We temporarily make new parties which get the necessary shares
// for a computation, this computation can then be parallalised. After the computation the resulting shares are given
// back to the original parties.

#define AESLEN 16

// The vector for the Sbox
static unsigned char forward_shift[8] = {
        241, 227, 199, 143, 31, 62, 124, 248
};

void SBoxFactory(Party* P0, unsigned long index1, unsigned long* index3) {

    Party* P0temp = new Party();
    Party *P1temp = new Party();
    Party* P2temp = new Party();

    P0temp->sync(P1temp, P2temp);
    P1temp->sync(P2temp, P0temp);
    P2temp->sync(P0temp, P1temp);

    P0temp->addShare(P0->getSharex(index1), P0->getSharea(index1));
    P1temp->addShare(P0->first_party->getSharex(index1), P0->first_party->getSharea(index1));
    unsigned long temp = P2temp->addShare(P0->second_party->getSharex(index1), P0->second_party->getSharea(index1));

    P0temp->power254(temp, &temp);
    P0temp->vectorMult(temp, forward_shift, &temp);
    P0temp->XORcons(temp, 99, &temp);

    if (*index3) {
        P0->setShare(*index3, P0temp->getSharex(temp), P0temp->getSharea(temp));
        P0->first_party->setShare(*index3, P1temp->getSharex(temp), P1temp->getSharea(temp));
        P0->second_party->setShare(*index3, P2temp->getSharex(temp), P2temp->getSharea(temp));
    } else {
        mtxopp.lock();
        P0->addShare(P0temp->getSharex(temp), P0temp->getSharea(temp));
        P0->first_party->addShare(P1temp->getSharex(temp), P1temp->getSharea(temp));
        *index3 = P0->second_party->addShare(P2temp->getSharex(temp), P2temp->getSharea(temp));
        mtxopp.unlock();
    }

    delete P0temp;
    delete P1temp;
    delete P2temp;
}

void KeyExpansionFactory(Party* P0, const unsigned long* K, unsigned long* RK) {
    unsigned long Key[AESLEN];
    unsigned long RoundKey[176];

    Party* P0temp = new Party();
    Party *P1temp = new Party();
    Party* P2temp = new Party();

    P0temp->sync(P1temp, P2temp);
    P1temp->sync(P2temp, P0temp);
    P2temp->sync(P0temp, P1temp);

    for (int i = 0; i < AESLEN; i++) {
        P0temp->addShare(P0->getSharex(K[i]), P0->getSharea(K[i]));
        P1temp->addShare(P0->first_party->getSharex(K[i]), P0->first_party->getSharea(K[i]));
        Key[i] = P2temp->addShare(P0->second_party->getSharex(K[i]), P0->second_party->getSharea(K[i]));
    }

    for (int i = 0; i < 176; i++) {
        P0temp->addShare(P0->getSharex(RK[i]), P0->getSharea(RK[i]));
        P1temp->addShare(P0->first_party->getSharex(RK[i]), P0->first_party->getSharea(RK[i]));
        RoundKey[i] = P2temp->addShare(P0->second_party->getSharex(RK[i]), P0->second_party->getSharea(RK[i]));
    }

    P0temp->KeyExpansion(Key, RoundKey);

    if (RK[0]) {
        for (int i = 0; i < 176; i++) {
            P0->setShare(RK[i], P0temp->getSharex(RoundKey[i]), P0temp->getSharea(RoundKey[i]));
            P0->first_party->setShare(RK[i], P1temp->getSharex(RoundKey[i]), P1temp->getSharea(RoundKey[i]));
            P0->second_party->setShare(RK[i], P2temp->getSharex(RoundKey[i]), P2temp->getSharea(RoundKey[i]));
        }
    } else {
        mtxopp.lock();
        for (int i = 0; i < 176; i++) {
            P0->addShare(P0temp->getSharex(RoundKey[i]), P0temp->getSharea(RoundKey[i]));
            P0->first_party->addShare(P1temp->getSharex(RoundKey[i]), P1temp->getSharea(RoundKey[i]));
            RK[i] = P0->second_party->addShare(P2temp->getSharex(RoundKey[i]), P2temp->getSharea(RoundKey[i]));
        }
        mtxopp.unlock();
    }

    delete P0temp;
    delete P1temp;
    delete P2temp;
}

void AESCTRFactory(Party* P0, unsigned long* M, const unsigned long* K, unsigned long C, unsigned long* index3) {
    unsigned long message[AESLEN];
    unsigned long encrypted_message[AESLEN];
    unsigned long key[176];

    Party* P0temp = new Party();
    Party *P1temp = new Party();
    Party* P2temp = new Party();

    P0temp->sync(P1temp, P2temp);
    P1temp->sync(P2temp, P0temp);
    P2temp->sync(P0temp, P1temp);

    for (int i = 0; i < AESLEN; i++) {
        P0temp->addShare(P0->getSharex(M[i]), P0->getSharea(M[i]));
        P1temp->addShare(P0->first_party->getSharex(M[i]), P0->first_party->getSharea(M[i]));
        message[i] = P2temp->addShare(P0->second_party->getSharex(M[i]), P0->second_party->getSharea(M[i]));
    }

    for (int i = 0; i < 176; i++) {
        P0temp->addShare(P0->getSharex(K[i]), P0->getSharea(K[i]));
        P1temp->addShare(P0->first_party->getSharex(K[i]), P0->first_party->getSharea(K[i]));
        key[i] = P2temp->addShare(P0->second_party->getSharex(K[i]), P0->second_party->getSharea(K[i]));
    }

    for (int i = 0; i < AESLEN; i++) {
        encrypted_message[i] = 0;
    }

    P0temp->AES_MPC_CTR(message, key, C, encrypted_message);

    if (index3[0]) {
        for (int i = 0; i < AESLEN; i++) {
            P0->setShare(index3[i], P0temp->getSharex(encrypted_message[i]), P0temp->getSharea(encrypted_message[i]));
            P0->first_party->setShare(index3[i], P1temp->getSharex(encrypted_message[i]), P1temp->getSharea(encrypted_message[i]));
            P0->second_party->setShare(index3[i], P2temp->getSharex(encrypted_message[i]), P2temp->getSharea(encrypted_message[i]));
        }
    } else {
        mtxopp.lock();
        for (int i = 0; i < AESLEN; i++) {
            P0->addShare(P0temp->getSharex(encrypted_message[i]), P0temp->getSharea(encrypted_message[i]));
            P0->first_party->addShare(P1temp->getSharex(encrypted_message[i]), P1temp->getSharea(encrypted_message[i]));
            index3[i] = P0->second_party->addShare(P2temp->getSharex(encrypted_message[i]), P2temp->getSharea(encrypted_message[i]));
        }
        mtxopp.unlock();
    }

    delete P0temp;
    delete P1temp;
    delete P2temp;
}



