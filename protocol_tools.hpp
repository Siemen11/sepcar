#ifndef FLOAN_PROTOCOL_TOOLS_HPP
#define FLOAN_PROTOCOL_TOOLS_HPP

#include <stdio.h>
#include <string.h>
#include <sqlite3.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "party.hpp"
#include "utils.hpp"

void receiveValue(unsigned char* out, unsigned char* in, unsigned int length);

unsigned long sendNewShares(Party* P1, Party* P2, Party* P3,unsigned char value);

void makeShareBytes(int length, unsigned char* value, unsigned char** resultx, unsigned char** resulta);

#endif //FLOAN_PROTOCOL_TOOLS_HPP
