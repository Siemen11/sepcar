cmake_minimum_required(VERSION 3.6)
project(SePCAR)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_COMPILER "g++")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L -lpthread -lcrypto -Wall -fexceptions -ldl -lssl -pthread -flto -O3 -g")
include_directories(/usr/include/openssl/)
link_libraries(ssl)
link_libraries(crypto)
link_libraries(pthread)
link_libraries(dl)

SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
SET(BUILD_SHARED_LIBRARIES OFF)
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")

set(SOURCE_FILES
        car.cpp
        car.hpp
        RSA.cpp
        RSA.hpp
        database.cpp
        database.hpp
        protocol_tools.cpp
        protocol_tools.hpp
        AES_MPC.cpp
        utils.cpp
        utils.hpp
        aes.cpp
        aes.hpp
        main.cpp
        party.cpp
        party.hpp
        operationFactory.cpp
        operationFactory.hpp)

add_executable(SePCAR ${SOURCE_FILES})

target_link_libraries(SePCAR LINK_PUBLIC sqlite3)