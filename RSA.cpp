#include "RSA.hpp"

int padding = RSA_PKCS1_PADDING;

bool generate_key(string name, int key_length)
{
    int             ret = 0;
    RSA             *r = NULL;
    BIGNUM          *bne = NULL;
    BIO             *bp_public = NULL, *bp_private = NULL;

    int             bits = key_length;
    unsigned long   e = RSA_F4;
    string          filename;

    // 1. generate rsa key
    bne = BN_new();
    ret = BN_set_word(bne,e);
    if(ret != 1){
        goto free_all;
    }

    r = RSA_new();
    ret = RSA_generate_key_ex(r, bits, bne, NULL);
    if(ret != 1){
        goto free_all;
    }

    // 2. save public key
    filename = name+"_public";
    bp_public = BIO_new_file(filename.c_str(), "w+");
    ret = PEM_write_bio_RSAPublicKey(bp_public, r);
    if(ret != 1){
        goto free_all;
    }

    // 3. save private key
    filename = name+"_private";
    bp_private = BIO_new_file(filename.c_str(), "w+");
    ret = PEM_write_bio_RSAPrivateKey(bp_private, r, NULL, NULL, 0, NULL, NULL);

    // 4. free
    free_all:

    BIO_free_all(bp_public);
    BIO_free_all(bp_private);
    RSA_free(r);
    BN_free(bne);

    return (ret == 1);
}

RSA * createRSA(unsigned char * key,int publicKey)
{
    RSA *rsa= NULL;
    BIO *keybio ;
    keybio = BIO_new_mem_buf(key, -1);
    if (keybio==NULL)
    {
        printf( "Failed to create key BIO");
        return 0;
    }
    if(publicKey)
    {
        rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa,NULL, NULL);
    }
    else
    {
        rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa,NULL, NULL);
    }
    if(rsa == NULL)
    {
        printf( "Failed to create RSA");
    }

    return rsa;
}

RSA * createRSAWithFilename(string filename,int publickey)
{
    if (publickey)
    {
        filename = filename+"_public";
    }
    else
    {
        filename = filename+"_private";
    }

    FILE * fp = fopen(filename.c_str(),"rb");

    if(fp == NULL)
    {
        printf("Unable to open file %s \n",filename.c_str());
        return NULL;
    }
    RSA *rsa= RSA_new() ;

    if(publickey)
    {
        rsa = PEM_read_RSA_PUBKEY(fp, &rsa,NULL, NULL);
    }
    else
    {
        rsa = PEM_read_RSAPrivateKey(fp, &rsa,NULL, NULL);
    }

    return rsa;
}

int public_encrypt(unsigned char * data,int data_len, string filename, unsigned char *encrypted)
{
    RSA * rsa = createRSAWithFilename(filename.c_str(),0);
    int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
    RSA_free(rsa);
    return result;
}

int private_decrypt(unsigned char * enc_data,int data_len, string filename, unsigned char *decrypted)
{
    RSA * rsa = createRSAWithFilename(filename.c_str(),0);
    int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
    RSA_free(rsa);
    return result;
}

int private_encrypt(unsigned char * data,int data_len, string filename, unsigned char *encrypted)
{
    RSA * rsa = createRSAWithFilename(filename.c_str(),0);
    int result = RSA_private_encrypt(data_len,data,encrypted,rsa,padding);
    RSA_free(rsa);
    return result;
}

int public_decrypt(unsigned char * enc_data,int data_len, string filename, unsigned char *decrypted)
{
    RSA * rsa = createRSAWithFilename(filename.c_str(),1);
    int  result = RSA_public_decrypt(data_len,enc_data,decrypted,rsa,padding);
    RSA_free(rsa);
    return result;
}

void printLastError(char *msg)
{
    char * err = (char*) malloc(130);;
    ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    printf("%s ERROR: %s\n",msg, err);
    free(err);
}
