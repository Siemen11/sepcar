#ifndef _AES_HPP_
#define _AES_HPP_

#include <stdint.h>
#include <ostream>
#include <iostream>


void AES_encrypt(const uint8_t* input, const uint8_t* Keytemp, uint8_t *output);
void AES_decrypt(const uint8_t *input, const uint8_t *Keytemp, uint8_t *output);

void AES_CBC_encrypt(uint8_t* output, uint8_t* input, uint32_t length, const uint8_t* Keytemp, const uint8_t* iv);
void AES_CBC_decrypt(uint8_t* output, uint8_t* input, uint32_t length, const uint8_t* Keytemp, const uint8_t* iv);

void AES_CTR(const uint8_t* input, const uint8_t* Keytemp, unsigned long counter, uint8_t* output);


#endif //_AES_H_
