# README #

* Quick summary: 
We present an efficient secure and privacy-enhancing protocol for car access provision, named SePCAR. The protocol is fully decentralised and allows users to share their cars conveniently in such a way that the security and privacy of the users is not sacrificed. It provides generation, update, revocation, and distribution mechanisms for access tokens to shared cars, as well as procedures to solve disputes and to deal with law enforcement requests, for instance in the case of car incidents. We prove that SePCAR meets its appropriate security and privacy requirements and that it is efficient: our practical efficiency analysis through a proof-of-concept implementation shows that SePCAR takes only 1.55 seconds for a car access provision.
* Run: 
The executable is a standalone static executable, making it possible to run it immediately on a Linux operating system.
Note that you also need the executable of the server as well as it is used to simulate the communication.

* Contact: 
siemen.dhooghe@esat.kuleuven.be