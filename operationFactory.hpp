#include "party.hpp"
#include <mutex>

using namespace std;

#ifndef FLOAN_OPERATIONFACTORY_HPP
#define FLOAN_OPERATIONFACTORY_HPP

void SBoxFactory(Party* P0, unsigned long index1, unsigned long* index3);

void KeyExpansionFactory(Party* P0, const unsigned long* K, unsigned long* RK);

void AESCTRFactory(Party* P0, unsigned long* M, const unsigned long* K, unsigned long C, unsigned long* index3);

#endif
