/*
This is an implementation of the AES128 algorithm, specifically ECB and CBC mode.
The implementation is verified against the test vectors in:
  National Institute of Standards and Technology Special Publication 800-38A 2001 ED
ECB-AES128
----------
  plain-text:
    6bc1bee22e409f96e93d7e117393172a
    ae2d8a571e03ac9c9eb76fac45af8e51
    30c81c46a35ce411e5fbc1191a0a52ef
    f69f2445df4f9b17ad2b417be66c3710
  K:
    2b7e151628aed2a6abf7158809cf4f3c
  resulting Cipher
    3ad77bb40d7a3660a89ecaf32466ef97
    f5d3d58503b9699de785895a96fdbaaf
    43b1cd7f598ece23881b00e3ed030688
    7b0c785e27e8ad3f8223207104725dd4
NOTE:   String length must be evenly divisible by 16byte (str_len % 16 == 0)
        You should pad the end of the string with zeros if this is not the case.
*/


/*****************************************************************************/
/* Includes:                                                                 */
/*****************************************************************************/
#include <stdint.h>
#include <string.h> // CBC mode, for memset
#include "aes.hpp"

using namespace std;

/*****************************************************************************/
/* Defines:                                                                  */
/*****************************************************************************/
// The number of columns comprising a actual in AES. This is a constant in AES. Value=4
#define Nb 4
// The number of 32 bit words in a K.
#define Nk 4
// K length in bytes [128 bit]
#define KLEN 16
// The number of rounds in AES Cipher.
#define Nr 10

/*****************************************************************************/
/* Private variables:                                                        */
/*****************************************************************************/
// actual - array holding the intermediate results during decryption.
typedef uint8_t state_t[4][4];

#ifndef MULTIPLY_AS_A_FUNCTION
#define MULTIPLY_AS_A_FUNCTION 0
#endif

// The lookup-tables are marked const so they can be placed in read-only storage instead of RAM
// The numbers below can be computed dynamically trading ROM for RAM -
// This can be useful in (embedded) bootloader applications, where ROM is often limited.
const uint8_t s_box[256] =   {
        //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

const uint8_t rs_box[256] =
        { 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
          0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
          0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
          0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
          0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
          0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
          0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
          0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
          0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
          0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
          0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
          0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
          0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
          0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
          0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
          0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };


// The round constant word array, R_con[i], contains the values given by
// x to th e power (i-1) being powers of x (x is denoted as {02}) in the field GF(2^8)
// Note that i starts at 1, not 0).
const uint8_t R_con[255] = {
        0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
        0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
        0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
        0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
        0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
        0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
        0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
        0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
        0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
        0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
        0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
        0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
        0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
        0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
        0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
        0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb  };


/*****************************************************************************/
/* Private functions:                                                        */
/*****************************************************************************/
uint8_t getSboxValue(uint8_t num)
{
    return s_box[num];
}

uint8_t getInverseSbox(uint8_t num)
{
    return rs_box[num];
}

// This function produces Nb(Nr+1) round Ks. The round Ks are used in each round to decrypt the actuals.
void KeyExpansion(const uint8_t *key, uint8_t *roundKey)
{
    uint32_t i, j;
    uint8_t tempa[4], k; // Used for the column/row operations

    // The first round key is the key itself.
    for(i = 0; i < Nk; ++i)
    {
        roundKey[(i * 4) + 0] = key[(i * 4) + 0];
        roundKey[(i * 4) + 1] = key[(i * 4) + 1];
        roundKey[(i * 4) + 2] = key[(i * 4) + 2];
        roundKey[(i * 4) + 3] = key[(i * 4) + 3];
    }
    // All other round Ks are found from the previous round Ks.
    for(; (i < (Nb * (Nr + 1))); ++i)
    {
        for(j = 0; j < 4; ++j)
        {
            tempa[j]=roundKey[(i-1) * 4 + j];
        }
        if (i % Nk == 0)
        {
            // This function rotates the 4 bytes in a word to the left once.
            // [a0,a1,a2,a3] becomes [a1,a2,a3,a0]

            // Function RotWord()
            {
                k = tempa[0];
                tempa[0] = tempa[1];
                tempa[1] = tempa[2];
                tempa[2] = tempa[3];
                tempa[3] = k;
            }

            // SubWord() is a function that takes a four-byte input word and
            // applies the S-box to each of the four bytes to produce an output word.

            // Function Subword()
            {
                tempa[0] = getSboxValue(tempa[0]);
                tempa[1] = getSboxValue(tempa[1]);
                tempa[2] = getSboxValue(tempa[2]);
                tempa[3] = getSboxValue(tempa[3]);
            }

            tempa[0] =  tempa[0] ^ R_con[i/Nk];
        }
        roundKey[i * 4 + 0] = roundKey[(i - Nk) * 4 + 0] ^ tempa[0];
        roundKey[i * 4 + 1] = roundKey[(i - Nk) * 4 + 1] ^ tempa[1];
        roundKey[i * 4 + 2] = roundKey[(i - Nk) * 4 + 2] ^ tempa[2];
        roundKey[i * 4 + 3] = roundKey[(i - Nk) * 4 + 3] ^ tempa[3];
    }
}

// This function adds the round K to actual.
// The round K is added to the actual by an XOR function.
void AddRoundKey(state_t *state, uint8_t *roundKey, uint8_t round)
{
    uint8_t i,j;
    for(i=0;i<4;++i)
    {
        for(j = 0; j < 4; ++j)
        {
            (*state)[i][j] ^= roundKey[round * Nb * 4 + i * Nb + j];
        }
    }
}

// The SubBytes Function Substitutes the values in the
// actual matrix with values in an S-box.
void SubBytes(state_t *state)
{
    uint8_t i, j;
    for(i = 0; i < 4; ++i)
    {
        for(j = 0; j < 4; ++j)
        {
            (*state)[j][i] = getSboxValue((*state)[j][i]);
        }
    }
}

// The ShiftRows() function shifts the rows in the actual to the left.
// Each row is shifted with different offset.
// Offset = Row number. So the first row is not shifted.
void ShiftRows(state_t *state)
{
    uint8_t temp;

    // Rotate first row 1 columns to left
    temp           = (*state)[0][1];
    (*state)[0][1] = (*state)[1][1];
    (*state)[1][1] = (*state)[2][1];
    (*state)[2][1] = (*state)[3][1];
    (*state)[3][1] = temp;

    // Rotate second row 2 columns to left
    temp           = (*state)[0][2];
    (*state)[0][2] = (*state)[2][2];
    (*state)[2][2] = temp;

    temp       = (*state)[1][2];
    (*state)[1][2] = (*state)[3][2];
    (*state)[3][2] = temp;

    // Rotate third row 3 columns to left
    temp       = (*state)[0][3];
    (*state)[0][3] = (*state)[3][3];
    (*state)[3][3] = (*state)[2][3];
    (*state)[2][3] = (*state)[1][3];
    (*state)[1][3] = temp;
}

uint8_t xtimes(uint8_t x)
{
    return (unsigned char)(((x<<1) ^ (((x>>7) & 1) * 0x1b))&0xFF);
}

// MixColumns function mixes the columns of the actual matrix
void MixColumns(state_t *state)
{
    uint8_t i;
    uint8_t Tmp,Tm,t;
    for(i = 0; i < 4; ++i)
    {
        t   = (*state)[i][0];
        Tmp = (*state)[i][0] ^ (*state)[i][1] ^ (*state)[i][2] ^ (*state)[i][3] ;
        Tm  = (*state)[i][0] ^ (*state)[i][1] ; Tm = xtimes(Tm);  (*state)[i][0] ^= Tm ^ Tmp ;
        Tm  = (*state)[i][1] ^ (*state)[i][2] ; Tm = xtimes(Tm);  (*state)[i][1] ^= Tm ^ Tmp ;
        Tm  = (*state)[i][2] ^ (*state)[i][3] ; Tm = xtimes(Tm);  (*state)[i][2] ^= Tm ^ Tmp ;
        Tm  = (*state)[i][3] ^ t ;        Tm = xtimes(Tm);  (*state)[i][3] ^= Tm ^ Tmp ;
    }
}

// Multiply is used to multiply numbers in the field GF(2^8)
#if MULTIPLY_AS_A_FUNCTION
static uint8_t Mult(uint8_t x, uint8_t y)
{
  return (((y & 1) * x) ^
       ((y>>1 & 1) * times(x)) ^
       ((y>>2 & 1) * times(times(x))) ^
       ((y>>3 & 1) * times(times(times(x)))) ^
       ((y>>4 & 1) * times(times(times(times(x))))));
  }
#else
#define Mult(x, y)                                   \
      (  ((y & 1) * x) ^                              \
      ((y>>1 & 1) * xtimes(x)) ^                       \
      ((y>>2 & 1) * xtimes(xtimes(x))) ^                \
      ((y>>3 & 1) * xtimes(xtimes(xtimes(x)))) ^         \
      ((y>>4 & 1) * xtimes(xtimes(xtimes(xtimes(x))))))   \

#endif

// MixColumns function mixes the columns of the actual matrix.
// The method used to multiply may be difficult to understand for the inexperienced.
// Please use the references to gain more information.
void InvMixColumns(state_t *state)
{
    int i;
    uint8_t a,b,c,d;
    for(i=0;i<4;++i)
    {
        a = (*state)[i][0];
        b = (*state)[i][1];
        c = (*state)[i][2];
        d = (*state)[i][3];

        (*state)[i][0] = (unsigned char)(Mult(a, 0x0e) ^ Mult(b, 0x0b) ^ Mult(c, 0x0d) ^ Mult(d, 0x09));
        (*state)[i][1] = (unsigned char)(Mult(a, 0x09) ^ Mult(b, 0x0e) ^ Mult(c, 0x0b) ^ Mult(d, 0x0d));
        (*state)[i][2] = (unsigned char)(Mult(a, 0x0d) ^ Mult(b, 0x09) ^ Mult(c, 0x0e) ^ Mult(d, 0x0b));
        (*state)[i][3] = (unsigned char)(Mult(a, 0x0b) ^ Mult(b, 0x0d) ^ Mult(c, 0x09) ^ Mult(d, 0x0e));
    }
}


// The SubBytes Function Substitutes the values in the
// actual matrix with values in an S-box.
void InvSubBytes(state_t *state)
{
    uint8_t i,j;
    for(i=0;i<4;++i)
    {
        for(j=0;j<4;++j)
        {
            (*state)[j][i] = getInverseSbox((*state)[j][i]);
        }
    }
}

void InvShiftRows(state_t *state)
{
    uint8_t temp;

    // Rotate first row 1 columns to right
    temp=(*state)[3][1];
    (*state)[3][1]=(*state)[2][1];
    (*state)[2][1]=(*state)[1][1];
    (*state)[1][1]=(*state)[0][1];
    (*state)[0][1]=temp;

    // Rotate second row 2 columns to right
    temp=(*state)[0][2];
    (*state)[0][2]=(*state)[2][2];
    (*state)[2][2]=temp;

    temp=(*state)[1][2];
    (*state)[1][2]=(*state)[3][2];
    (*state)[3][2]=temp;

    // Rotate third row 3 columns to right
    temp=(*state)[0][3];
    (*state)[0][3]=(*state)[1][3];
    (*state)[1][3]=(*state)[2][3];
    (*state)[2][3]=(*state)[3][3];
    (*state)[3][3]=temp;
}

// Cipher is the main function that encrypts the PlainText.
void Cipher(state_t *state, uint8_t *roundKey)
{
    uint8_t round = 0;

    // Add the First round K to the state before starting the rounds.
    AddRoundKey(state, roundKey, 0);

    // There will be Nr rounds.
    // The first Nr-1 rounds are identical.
    // These Nr-1 rounds are executed in the loop below.
    for(round = 1; round < Nr; ++round)
    {
        SubBytes(state);
        ShiftRows(state);
        MixColumns(state);
        AddRoundKey(state, roundKey, round);
    }

    // The last round is given below.
    // The MixColumns function is not here in the last round.
    SubBytes(state);
    ShiftRows(state);
    AddRoundKey(state, roundKey, Nr);
}

void InvCipher(state_t *state, uint8_t *roundKey)
{
    uint8_t round=0;

    // Add the First round K to the state before starting the rounds.
    AddRoundKey(state, roundKey, Nr);

    // There will be Nr rounds.
    // The first Nr-1 rounds are identical.
    // These Nr-1 rounds are executed in the loop below.
    for(round=Nr-1;round>0;round--)
    {
        InvShiftRows(state);
        InvSubBytes(state);
        AddRoundKey(state, roundKey, round);
        InvMixColumns(state);
    }

    // The last round is given below.
    // The MixColumns function is not here in the last round.
    InvShiftRows(state);
    InvSubBytes(state);
    AddRoundKey(state, roundKey, 0);
}

void BlockCopy(uint8_t *output, const uint8_t *input)
{
    uint8_t i;
    for (i=0;i<KLEN;++i)
    {
        output[i] = input[i];
    }
}

/*****************************************************************************/
/* Public functions:                                                         */
/*****************************************************************************/


void AES_encrypt(const uint8_t* input, const uint8_t* Keytemp, uint8_t* output)
{
    // Copy input to output, and work in-memory on output
    BlockCopy(output, input);
    state_t* state;
    state = (state_t*)output;

    uint8_t roundKey[176];

    const uint8_t* key;

    key = Keytemp;
    KeyExpansion(key, roundKey);

    // The next function call encrypts the PlainText with the key using AES algorithm.
    Cipher(state, roundKey);
}

void AES_decrypt(const uint8_t* input, const uint8_t* Keytemp, uint8_t *output)
{
    // Copy input to output, and work in-memory on output
    BlockCopy(output, input);
    state_t* state;
    state = (state_t*)output;

    uint8_t roundKey[176];

    const uint8_t* key;

    // The KeyExpansion routine must be called before encryption.
    key = Keytemp;
    KeyExpansion(key, roundKey);

    InvCipher(state, roundKey);
}


void XorWIv(uint8_t* IV, uint8_t* buf)
{
    uint8_t i;
    for(i = 0; i < KLEN; ++i)
    {
        buf[i] ^= IV[i];
    }
}

void AES_CBC_encrypt(uint8_t* output, uint8_t* input, uint32_t length, const uint8_t* Keytemp, const uint8_t* iv)
{
    uintptr_t i;

    BlockCopy(output, input);
    state_t* state;

    uint8_t roundKey[176];

    const uint8_t* key;

    key = Keytemp;
    KeyExpansion(key, roundKey);

    uint8_t* IV;
    IV = (uint8_t*)iv;

    for(i = 0; i < length; i += KLEN)
    {
        XorWIv(IV, input);
        BlockCopy(output, input);
        state = (state_t*)output;
        Cipher(state, roundKey);
        IV = output;
        input += KLEN;
        output += KLEN;
    }
}

void AES_CBC_decrypt(uint8_t* output, uint8_t* input, uint32_t length, const uint8_t* Keytemp, const uint8_t* iv)
{
    uintptr_t i;
    uint8_t remainders = (unsigned char)(length % KLEN); /* Remaining bytes in the last non-full block */

    BlockCopy(output, input);
    state_t* state;

    uint8_t* IV;

    const uint8_t* key;

    uint8_t roundKey[176];

    // Skip the key expansion if key is passed as 0
    if(0 != Keytemp)
    {
        key = Keytemp;
        KeyExpansion(key, roundKey);
    }

    IV = (uint8_t*)iv;

    for(i = 0; i < length; i += KLEN)
    {
        BlockCopy(output, input);
        state = (state_t*)output;
        InvCipher(state, roundKey);
        XorWIv(IV, output);
        IV = input;
        input += KLEN;
        output += KLEN;
    }

    if(remainders)
    {
        BlockCopy(output, input);
        memset(output+remainders, 0, (unsigned int)(KLEN - remainders)); /* add 0-padding */
        state = (state_t*)output;
        InvCipher(state, roundKey);
    }
}

void AES_CTR(const uint8_t* input, const uint8_t* Keytemp, unsigned long counter, uint8_t* output)
{
    // Make a character array out of the integer
    uint8_t counter_array[KLEN];
    for (int i = 0; i < 4; i++) {
        counter_array[i] = (unsigned char) ((counter>>(8*i))&0xFF);
    }
    for (int i = 4; i < KLEN; i++) {
        counter_array[i] = 0;
    }

    // Copy input to output, and work in-memory on output
    BlockCopy(output, counter_array);
    state_t* state;
    state = (state_t*)output;

    const uint8_t* key;

    key = Keytemp;
    uint8_t roundKey[176];
    KeyExpansion(key, roundKey);

    // The next function call encrypts the PlainText with the key using AES algorithm.
    Cipher(state, roundKey);

    // XOR the input with the encrypted output of the counter
    for(int i=0;i<4;++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            (*state)[i][j] ^= input[4*i+j];
        }
    }
}
